const express = require('express');
const nanoid = require('nanoid');

const createRouter = connection => {
    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `comments`', (error,results) =>{
            if (error){
                res.status(500).send({error:'Database error'});
            }
            res.send(results);
        });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `comments` WHERE `news_id` = ?',req.params.id, (error,results) =>{
            console.log(results);
            if (error){
                res.status(500).send({error:'Database error'});
            }
            res.send(results);
        });
    });

    router.post('/', (req, res) => {
        const comments = req.body;
        connection.query('INSERT INTO `comments` (`news_id`, `author`,`comment`) VALUES (?, ?, ?)',
            [comments.news_id, comments.author,comments.comment],
            (error, results) => {
                if (error){
                    console.log(error);
                    res.status(500).send({error:'Database error'});
                }

                res.send({message: 'OK'});
            }
        );
    });

    router.delete('/:id', (req, res) => {//функция не прописана
        connection.query('DELETE FROM `comments` WHERE `id` = ?',req.params.id, (error,results) =>{
            if (error){
                res.status(500).send({error:'Database error'});
            }
            res.send({message: 'OK'});
        });
    });

    return router;
};




module.exports = createRouter;