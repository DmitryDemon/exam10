const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = connection => {

    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `news`', (error,results) =>{
            if (error){
                res.status(500).send({error:'Database error'});
            }
            res.send(results);
        });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `news` WHERE `id` = ?',req.params.id, (error,results) =>{
            if (error){
                res.status(500).send({error:'Database error'});
            }
            res.send(results);
        });
    });

    router.post('/', upload.single('image'), (req, res) => {
        const oneNews = req.body;

        const date = new Date().toISOString();
        oneNews.publication_date = date;

        if (req.file) {
            oneNews.image = req.file.filename;
        }

        connection.query('INSERT INTO `news` (`title`, `content`, `image`, `publication_date`) VALUES (?, ?, ?, ?)',
            [oneNews.title, oneNews.content, oneNews.image, oneNews.publication_date],
            (error, results) => {
                if (error){
                    console.log(error);
                    res.status(500).send({error:'Database error'});
                }

                res.send({message: 'OK'});
            }
        );
    });

    router.delete('/:id', (req, res) => {//функция не прописана
        connection.query('DELETE FROM `news` WHERE `id` = ?',req.params.id, (error,results) =>{
            if (error){
                res.status(500).send({error:'Database error'});
            }
            res.send({message: 'OK'});
        });
    });

    return router;
};






module.exports = createRouter;