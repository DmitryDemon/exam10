 CREATE SCHEMA `news_site` DEFAULT CHARACTER SET utf8 ;

 USE `news_site`;

 CREATE TABLE `news` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`title` VARCHAR(255) NOT NULL,
	`content` TEXT NULL NOT NULL,
	`image` VARCHAR(100) NULL,
	`publication_date` VARCHAR(40) NOT NULL
);

CREATE TABLE `comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `news_id` INT NOT NULL,
  `author` VARCHAR(45) NULL,
  `comment` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_comments_1_idx` (`news_id` ASC),
  CONSTRAINT `fk_comments_1`
    FOREIGN KEY (`news_id`)
    REFERENCES `news` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

INSERT INTO `news` (`id`,`title`,`content`,`image`,`publication_date`)
VALUES
	(1,'Hello World','The PM said the UK would need fter her plan',NULL,'10.04.2019'),
	(2,'Atention','Computers of various types','good','4.02.2019'),
	(3,'Weather','Mechanical appliances that perform some household functions','nice','22.03.2019')
;

INSERT INTO `comments` (`id`,`news_id`,`author`,`comment`)
VALUES
	(1,1,'John Dou','Good news'),
	(2,2,'Sone guy','some shit'),
	(3,3,'Jim Dim','alcohol is gooooood')
;
    
    