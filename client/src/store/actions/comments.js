import {FETCH_COMMENTS_SUCCESS} from "./actionTypes";
import axios from '../../axios-api';

export const fetchCommentsSuccess = comments => { return {type: FETCH_COMMENTS_SUCCESS, comments};
};
export const CREATE_COMMENTS_SUCCESS = 'CREATE_COMMENTS_SUCCESS';


export const createCommentsSuccess = () => {
    return {type: CREATE_COMMENTS_SUCCESS};
};

export const fetchComments = (id) => {
    return dispatch => {
        return axios.get('/comments/' + id).then(
            response => {
                return dispatch(fetchCommentsSuccess(response.data))
            },
            error => console.log(error.response.data)
        );
    };
};

export const createComment = (commentData) => {
    return dispatch => {
        return axios.post('/comments', commentData).then(
            response => dispatch(createCommentsSuccess())
        );
    };
};

export const deleteComment = (commentData) => {
    return dispatch => {
        return axios.delete('comments/' + commentData).then(
            response => dispatch(createCommentsSuccess())

        );
    }
};