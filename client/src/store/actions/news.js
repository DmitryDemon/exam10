import {FETCH_NEWS_SUCCESS} from "./actionTypes";
import axios from '../../axios-api';
import {createCommentsSuccess} from "./comments";

export const fetchNewsSuccess = news => {return {type: FETCH_NEWS_SUCCESS, news};
};
export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';


export const createNewsSuccess = () => {
    return {type: CREATE_NEWS_SUCCESS};
};



export const fetchNews = () => {
    return dispatch => {
        return axios.get('/news').then(
            response => dispatch(fetchNewsSuccess(response.data))
        );
    };
};



export const fetchOneNews = id => {
    return dispatch => {
        return axios.get('/news/'+id).then(
            response => {
                return dispatch(fetchNewsSuccess(response.data))
            }
        );
    };
};

export const createNews = (newsData) => {
    return dispatch => {
        return axios.post('/news', newsData).then(
            response => dispatch(createNewsSuccess())
        );
    };
};

export const deleteNews = (newsData) => {
    return dispatch => {
        return axios.delete('news/' + newsData).then(
            response => dispatch(createNewsSuccess())

        );
    }
};