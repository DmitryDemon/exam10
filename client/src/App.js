import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import {Container} from "reactstrap";

import Toolbar from "./components/UI/Toolbar/Toolbar";
import NewNews from "./containers/NewNews/NewNews";
import News from "./containers/News/News";
import OneNews from "./containers/OneNews/OneNews";

class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Toolbar/>
                </header>
                <Container style={{marginTop: '20px'}}>
                    <Switch>
                        <Route path="/" exact component={News} />
                        <Route path="/news/new" exact component={NewNews} />
                        <Route path="/news/:id" exact component={OneNews} />

                    </Switch>
                </Container>
            </Fragment>
        );
    }
}



export default App;
