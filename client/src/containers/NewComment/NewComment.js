import React, {Component, Fragment} from 'react';
import CommentForm from "../../components/CommentForm/CommentForm";
import {connect} from "react-redux";
import {createComment} from "../../store/actions/comments";

class NewNews extends Component {

    createComment = commentData => {
        this.props.onCommentCreated(commentData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>Add comment</h2>
                <CommentForm onSubmit={this.createComment} />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCommentCreated: commentData => dispatch(createComment(commentData))
    }
};



export default connect(null, mapDispatchToProps)(NewNews);